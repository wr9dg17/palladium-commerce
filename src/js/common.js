// Styles
import "../css/range.css";
import "selectize/dist/css/selectize.default.css";
import "aos/dist/aos.css";
import "swiper/dist/css/swiper.min.css";
import "../scss/style.scss";
// JS
import "selectize/dist/js/selectize";
import "./libs/range.min.js";
import AOS from "aos";
import Swiper from "swiper";

$(document).ready(() => {
    // AOS init
    AOS.init({
        once: true
    });

    // Selectize Init
    const customSelect = $(".custom-select");

    if ( customSelect.length > 0 ) {
        customSelect.selectize();
        $(".selectize-control").find("input").attr("disabled", true);
    }

    // Menu toggle
    const sideMenu = $(".side-menu");
    const menuToggler = $(".menu-toggler");

    menuToggler.click(function() {
        $(this).toggleClass("active");
        sideMenu.toggleClass("active");
    });

    // Search toggle
    const searchField = $(".search-field");
    const triggerSearch = $(".trigger-search");
    const closeSearchField = $(".close-search-field");
    const searchResults = $(".search-results");

    triggerSearch.click(() => {
        searchField.fadeIn(150);
        searchField.find("input").focus();
    });

    closeSearchField.click(() => {
        searchField.fadeOut(150);
        searchField.find("input").val("");
        searchResults.removeClass("active");
    });

    // Intro slider init
    const introSlider = new Swiper(".intro-slider", {
        autoplay: {
            delay: 7000
        },
        pagination: {
            el: '.swiper-pagination',
            type: 'bullets',
            clickable: true
        },
        lazy: {
            loadPrevNext: true,
            loadPrevNextAmount: 1,
            preloadImages: false
        }
    });

    // Brands slides init
    const brandsSlider = new Swiper(".brands-slider", {
        autoplay: {
            delay: 4000
        },
        slidesPerView: 8,
        spaceBetween: 20,
        breakpoints: {
            1280: {
                slidesPerView: 6
            },
            991: {
                slidesPerView: 4
            },
            650: {
                slidesPerView: 3
            },
            480: {
                slidesPerView: 2,
                spaceBetween: 15
            }
        }
    });

    // Suggested products
    const prevSuggestedProduct = $(".suggested-products .slide-prev");
    const nextSuggestedProduct = $(".suggested-products .slide-next");
    const colorChecker = $(".available-colors .color");

    const suggestedProducts = new Swiper(".products-slider", {
        slidesPerView: 4,
        spaceBetween: 24,
        autoplay: {
            delay: 7000
        },
        pagination: {
            el: '.swiper-pagination',
            type: 'bullets',
            clickable: true
        },
        breakpoints: {
            1280: {
                slidesPerView: 3,
                spaceBetween: 20
            },
            767: {
                slidesPerView: 2,
                spaceBetween: 10
            },
            500: {
                slidesPerView: 1
            }
        }
    });

    prevSuggestedProduct.click(() => {
        suggestedProducts.slidePrev();
    });

    nextSuggestedProduct.click(() => {
        suggestedProducts.slideNext();
    });

    // *************************** 

    colorChecker.on("click", function() {
        const index = $(this).index();
        const productImage = $(this).closest(".product-card").find(".image");

        $(this).closest(".available-colors").find(".color").removeClass("active");
        productImage.removeClass("active");
        $(this).addClass("active");
        productImage.eq(index).addClass("active");
    });

    // ****************************

    const subscribeField = $(".subscribe-form input");

    subscribeField.focusin(() => {
        subscribeField.closest(".form-group").addClass("focused");
    });

    subscribeField.focusout(() => {
        subscribeField.closest(".form-group").removeClass("focused");
    });

    // Products page
    const topBanners = new Swiper(".top-bnrs .swiper-container", {
        autoplay: {
            delay: 4000
        },
        slidesPerView: 3,
        spaceBetween: 24,
        noSwipingClass: "no-swipe",
        lazy: {
            loadPrevNext: true,
            loadPrevNextAmount: 1,
            preloadImages: false
        },
        breakpoints: {
            1024: {
                slidesPerView: 2,
                noSwipingClass: "allow-swipe"
            },
            767: {
                slidesPerView: 1,
                noSwipingClass: "allow-swipe"
            }
        }
    });

    // **************************

    const rangeSlider = $(".range-input");

    if (rangeSlider.length > 0) {
        const min = rangeSlider.data("min");
        const max = rangeSlider.data("max");

        rangeSlider.jRange({
            from: parseFloat(min),
            to: parseFloat(max),
            step: 1,
            isRange : true
        });
    };

    // ***************************

    const filtersWrapper = $(".filters-wrapper");
    const toggleFilters = $(".toggle-filters");

    toggleFilters.click(() => {
        filtersWrapper.toggleClass("active");
        contentOverlay.fadeToggle(200);
    });

    // Product inner page
    const galleryThumbs = new Swiper(".gallery-thumbs .swiper-container", {
        direction: "vertical",
        slidesPerView: 6,
        spaceBetween: 15,
        watchSlidesVisibility: true,
        watchSlidesProgress: true,
        lazy: {
            loadPrevNext: true,
            loadPrevNextAmount: 1,
            preloadImages: false
        },
        breakpoints: {
            575: {
                direction: "horizontal"
            },
            480: {
                direction: "horizontal",
                slidesPerView: 5
            },
            380: {
                direction: "horizontal",
                slidesPerView: 4
            }
        }
    });

    const gallery = new Swiper(".gallery .swiper-container", {
        effect: "fade",
        thumbs: {
            swiper: galleryThumbs
        },
        lazy: {
            loadPrevNext: true,
            loadPrevNextAmount: 1,
            preloadImages: false
        }
    });

    const colorsSlider = new Swiper(".colors-slider", {
        slidesPerView: "auto",
        spaceBetween: 15,
        noSwipingClass: "no-swipe",
        breakpoints: {
            480: {
                noSwipingClass: "allow-swipe"
            }
        }
    });

    // User Account
    const accoutSiderbarWrapper = $(".account-sidebar-wrapper");
    const toggleAccountSidebar = $(".toggle-account-sidebar");

    toggleAccountSidebar.click(() => {
        accoutSiderbarWrapper.toggleClass("active");
        contentOverlay.fadeToggle(200);
    });

    // Content overlay click handler
    const contentOverlay = $(".content-overlay");

    contentOverlay.click(function() {
        $(this).fadeOut(200);
        filtersWrapper.removeClass("active");
        accoutSiderbarWrapper.removeClass("active");
    });

    // Hiding alerts
    const siteAlert = $(".site-alert");
    
    if (siteAlert.length > 0) {
        setTimeout(() => {
            siteAlert.remove();
        }, 7000);
    };

    // Check if inputs and selects are empty
    const inputField = $("input.form-control");
    
    if (inputField.length > 0) {
        inputField.each(function() {
            inputIsFilled($(this));
        });
    }

    inputField.on("change", function() {
        inputIsFilled($(this));
    });

    function inputIsFilled(el) {
        let value = el.val();

        if (value.trim() !== "") {
            el.addClass("filled");
        } else {
            if (el.hasClass("filled")) {
                el.removeClass("filled");
            }
        }        
    };

    // ***************************

    const selectOption = $("select.custom-select");

    if (selectOption.length > 0) {
        selectOption.each(function() {
            selectIsSelected($(this));
        });
    }

    selectOption.on("change", function() {
        selectIsSelected($(this));
    })

    function selectIsSelected(el) {
        let value = el.val();
        let selectizeControl = el.next(".selectize-control");

        if (value.trim() !== "") {
            selectizeControl.addClass("filled");
        } else {
            if (selectizeControl.hasClass("filled")) {
                selectizeControl.removeClass("filled");
            }
        }
    }

});
